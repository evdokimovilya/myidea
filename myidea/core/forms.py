from django import forms
from myidea.core.models import Idea,User, Category
from myidea.core import models


class AddIdeaFrom(forms.ModelForm):
	class Meta:
		model = Idea
		fields = ['title', 'description', 'image']
	title = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control'}))
	description = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))
	image = forms.FileField(required=False)


class RejectForm(forms.ModelForm):
	class Meta:
		model = Idea
		fields = ['comment']
	comment = forms.CharField(max_length=500,widget=forms.Textarea(attrs={'class':'form_control'}))



class ApproveForm(forms.ModelForm):

	class Meta:
		model = Idea
		fields = ['category']

	category = forms.ModelMultipleChoiceField(Category.objects.all(), widget=forms.CheckboxSelectMultiple(), required=True, error_messages={'required': 'Нужно выбрать категорию!'})

class ResultForm(forms.ModelForm):
	class Meta:
		model = Idea
		fields = ['result']
	result = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'class': 'form-control'}))