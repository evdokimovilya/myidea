from django.core.management.base import BaseCommand, CommandError
from myidea.core.models import Idea,Status,Vote,  STATUS_FAIL, STATUS_APPROVED, STATUS_COMPLETE
from datetime import date
from myidea.core.views import my_send_mail
from django.db.models import Count, Case, When

class Command(BaseCommand):
	def handle(self, *args, **options):
		ideas = Idea.objects.annotate(
			count_vote_y=Count(Case(When(vote__choice=True, then=1))),
			count_vote_n=Count(Case(When(vote__choice=False, then=0)))).filter(status__name=STATUS_APPROVED)   #Все инициативы на голосовании
		vote_period = 6
		vote_y = 1
		vote_n = 1
		ideas_count_y = 0
		ideas_count_n = 0
		status_fail = Status.objects.get(name = STATUS_FAIL)
		status_complete = Status.objects.get(name=STATUS_COMPLETE)
		date_now = date.today()
		for idea in ideas:
			vote_date = idea.vote_date.date()
			idea_vote_period = (date_now - vote_date).days
			if idea_vote_period > vote_period: # если время  закончилось
				if idea.count_vote_y >= vote_y and idea.count_vote_n < vote_n: # если пройден порог голосов "против"  и пройден порог голосов "за"
					idea.status = status_complete
					idea.save()
					ideas_count_y = ideas_count_y + 1
					self.stdout.write("Инициатива %s прошла голосование" % idea.id)
					my_send_mail('idea_complete', idea)
				else: # если не пройден порог голосов "за" или пройден порого гослов "против"
					idea.status = status_fail
					idea.save()
					my_send_mail('idea_fail', idea)
					ideas_count_n = ideas_count_n + 1
					self.stdout.write("Инициатива %s не прошла голосование" % idea.id)
		self.stdout.write("Прошло голосование %s инициатив" % ideas_count_y)
		self.stdout.write("Инициатив закрыто %s инициатив" % ideas_count_n)



