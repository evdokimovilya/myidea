from django import template

register = template.Library()


@register.inclusion_tag('myidea/bootstrap_paginator.html')
def bootstrap_paginator(count_of_pages, current_page_number):
	wrap = 2
	head = 1 + wrap  # show nums from first page to this
	tail = count_of_pages - wrap  # show nums from this page to last
	below = current_page_number - wrap
	above = current_page_number + wrap
	advanced_page_range = []
	dots_appeard = False
	for page_number in range(1, count_of_pages + 1):
		if page_number < head or page_number > tail or page_number > below and page_number < above:
			dots_appeard = False
			advanced_page_range.append(page_number)
		elif not dots_appeard:
			dots_appeard = True
			advanced_page_range.append('.')
	c = {
		'pages': advanced_page_range,
		'selected': current_page_number,
	}
	return c
