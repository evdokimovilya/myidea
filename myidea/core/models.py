# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserInformation(models.Model):
	""" дополнительная информация о пользователе """
	user = models.OneToOneField(User, primary_key=True)
	course = models.IntegerField()
	institute = models.CharField(max_length=50)
	groupname = models.CharField(max_length=15)
	def __str__(self):
		return self.user.username
	class Meta:
		db_table = "ui_userinfo"


class Status(models.Model):
	""" статусы инициатив """
	name = models.CharField(max_length=30)
	def __str__(self):
		return self.name
	class Meta:
		db_table = 'ui_status'

STATUS = {
	'STATUS_APPROVED':	'На голосовании',
	'STATUS_REJECTED':	'Отклонена',
	'STATUS_REVIEW':	'На проверке',
	'STATUS_FAIL':		'Не прошло голосование',
	'STATUS_COMPLETE':	'Голосование успешно завершено',
	'STATUS_RESULT':	'Результат',
}

CATEGORY = {
	'information_policy':			'Информационная политика',
	'it':							'Информационные технологии',
	'intermational_connections':	'Международные связи',
	'general':						'Общие вопросы',
	'science':						'Наука',
	'education':					'Учебная работа',
	'economy':						'Экономика и стратегическое развитие',
}

STATUS_APPROVED = 'На голосовании'
STATUS_REJECTED = 'Отклонена'
STATUS_REVIEW = 'На проверке'
STATUS_FAIL = 'Не прошло голосование'
STATUS_COMPLETE = 'Голосование успешно завершено'
STATUS_RESULT = 'Результат'


class Category(models.Model):
	""" категории для размещения инициатив """
	name = models.CharField(max_length=50)
	def __str__(self):
		return  self.name
	class Meta():
		db_table = 'ui_category'


class Idea(models.Model):
	""" инициативы """
	user = models.ForeignKey(User)
	create_date = models.DateTimeField(auto_now_add=True)
	vote_date = models.DateTimeField(null=True, blank=True)
	title = models.CharField(max_length=100)
	description = models.TextField()
	image = models.FileField(max_length=255, upload_to='ideas/%Y/%m/%d/', blank=True, null=True)
	status = models.ForeignKey(Status, blank=True)
	category = models.ManyToManyField(Category, db_table='ui_idea_category', blank=True)
	comment = models.TextField(blank=True)
	user_moderate = models.ForeignKey(User, related_name='user_ideas', blank=True, null=True)
	result = models.TextField(blank=True)

	def __str__(self):
		return self.title
	class Meta:
		db_table = "ui_idea"
		ordering = ['-vote_date', '-create_date']


class Vote(models.Model):
	""" голосование (кто, когда, как) """
	idea = models.ForeignKey(Idea) 
	user = models.ForeignKey(User)
	choice = models.BooleanField()
	vote_date = models.DateTimeField(auto_now_add=True) 
	ip = models.GenericIPAddressField() 
	def __str__(self):
		return self.user.username
	class Meta():
		db_table = 'ui_vote'
