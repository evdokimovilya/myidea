from django.conf.urls import url, include
from . import views

urlpatterns = [
	url(r'^$',											views.main_idea_list,			name='index'),

	url(r'^ideas/', include([
		url(r'^add/',									views.addidea,					name='addidea'),
		url(r'^(?P<idea_id>\d+)/', include([
			url(r'^$',									views.idea_detail,				name='idea_detail'),
			url(r'^like/$',								views.like,                 	name='like'),
			url(r'^dislike/$',							views.dislike,             		name='dislike'),
		])),
		url(r'^(?P<section>\w+)/', include([
			url(r'^$',									views.main_idea_list, 					name='main_idea_list'),
			url(r'^(?P<sort_way>\w+)/$',				views.main_idea_list, 					name='main_idea_list'),
			url(r'^(?P<sort_way>\w+)/(?P<cat>\w+)/$',	views.main_idea_list,					name='main_idea_list'),
		])),
		url(r'^(\w+)/(\w+)/$',           				views.main_idea_list, 					name='main_idea_list'),
	])),

	url(r'^my/ideas/', include([
		url(r'^$',                      				views.user_idea_list,         		name='user'),
		url(r'^(?P<idea_id>\d+)/$',						views.user_idea_detail,				name='user_idea_detail'),
		url(r'^(?P<select_status>\w+)/$',				views.user_idea_list, 				name='user_idea_list'),
		

	])),
	
	url(r'^moderator/ideas/', include([
		url(r'^$',										views.moderator_idea_list,				name='moderator'),
		url(r'^(?P<idea_id>\d+)/',						include([
			url(r'^$',									views.moderator_idea_detail,			name='moderator_idea_detail'),
			url(r'^approve/$',							views.approve,  						name='approve'),  
			url(r'^reject/$',							views.reject,  							name='reject'),
			url(r'^result/$',							views.result,  							name='result'),
		])),
		url(r'^(?P<select_status>\w+)/$',				views.moderator_idea_list, 				name='moderator_idea_list'),
	])),
]