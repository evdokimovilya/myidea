
/*

в HTML файле должно быть следующие элементы:

<div id="vote_area">
	<button id="like">Я поддерживаю</button>
	<button id="dislike">Я против</button>
</div>


 */

$(document).ready(function() {
	
	$('#like').click(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'post', // обязательно указываем тип HTTP запроса - POST
			url: 'like/' , // адрес, на который отправляется запрос
			dataType: 'html', // тип возвращаемых данных
			success: function(response) { // функция будет вызвана при успешном выполнении
				$('#vote_area').html(response);  // заменяет содержимое элемента с id="vote_area" на ответ сервера
			},
			error: function(response) { // функция будет вызвана при ошибке
				alert('ОШИБКА!'); 
			}
		});
	});
	
	$('#dislike').click(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'post',
			url: "dislike/",
			dataType: 'html',
			success: function(response) {
				$('#vote_area').html(response);
			},
			error: function(response) {
				alert('ОШИБКА!');
			}
		});
	});	

});
