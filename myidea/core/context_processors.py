from .models import Idea

def user_count_idea(request):
	return {"user_count_idea": Idea.objects.filter(user__id = request.user.id).count()}