
from django.contrib import admin
from .models import UserInformation, Status, Category, Idea, Vote

admin.site.register(UserInformation)
admin.site.register(Status)
admin.site.register(Idea)
admin.site.register(Category)
admin.site.register(Vote)
# Register your models here.
