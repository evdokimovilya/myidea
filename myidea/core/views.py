# -*- coding: utf-8 -*-
from datetime import datetime, date

from django.contrib import auth
from django.contrib.auth.decorators import  login_required
from django.core.mail import EmailMultiAlternatives
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, Case, When
from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.utils.html import strip_tags

from .models import STATUS, CATEGORY
from .models import STATUS_APPROVED, STATUS_REJECTED, STATUS_REVIEW, STATUS_RESULT #, STATUS_COMPLETE, STATUS_FAIL
from .models import Idea, Vote, Status
from .forms import AddIdeaFrom, ApproveForm, RejectForm, ResultForm

def main_idea_list(request, section='onvote', sort_way='popular', cat='all_category'):

	ideas = Idea.objects.annotate(
		count_vote_y=Count(Case(When(vote__choice=True, then=1))),
		count_vote_n=Count(Case(When(vote__choice=False, then=0)))
	)

	if section == 'result':
		ideas = ideas.filter(status__name=STATUS_RESULT)
	if section == 'onvote':
		ideas = ideas.filter(status__name=STATUS_APPROVED)

	if sort_way == 'popular':
		ideas = ideas.order_by('-count_vote_y')
	if sort_way == 'date':
		ideas = ideas.order_by('-vote_date')

	if cat in CATEGORY.keys():
		ideas = ideas.filter(category__name = CATEGORY[cat])

	paginator = Paginator(ideas, 10)
	page = request.GET.get('page')
	count_ideas = paginator.count

	try:
		current_page = paginator.page(page)
	except PageNotAnInteger:
		current_page = paginator.page(1)
	except EmptyPage:
		current_page = paginator.page(paginator.num_pages)
	c = { 'current_page': current_page, 'status':STATUS, 'sort_way':sort_way, 'section':section, 'cat':cat, "CATEGORY":CATEGORY, 'count_ideas':count_ideas }
	return render(request, 'myidea/index.html', c)


@login_required()
def addidea(request):
	if request.method == "POST":
		form = AddIdeaFrom(request.POST, request.FILES)
		if form.is_valid():
			idea = form.save(commit=False)
			idea.user = request.user
			idea.create_date = datetime.now()
			new_status = Status.objects.get(name=STATUS_REVIEW )
			idea.status = new_status
			my_send_mail('idea_review', idea)
			idea.save()

			return redirect('/my/ideas')
	else:
		form = AddIdeaFrom()

	return render(request, 'myidea/my/add_idea.html', {'form': form})


def idea_detail(request, idea_id):
	idea = Idea.objects.annotate(
		count_vote_y=Count(Case(When(vote__choice=True, then=1))),
		count_vote_n=Count(Case(When(vote__choice=False, then=0)))
	).get(id=idea_id)

	vote_user = Vote.objects.filter(user__id=request.user.id).values_list('idea_id', flat=True)
	user_choice = False

	date_now = date.today()
	vote_date = idea.vote_date.date()
	idea_vote_period = (date_now - vote_date).days
	last_period = 31 - idea_vote_period

	if idea.id in vote_user:
		user_vote = Vote.objects.get(idea_id=idea.id, user=request.user)
		user_choice = user_vote.choice

	c = { 'idea': idea, 'vote_user': vote_user, 'user_choice': user_choice, 'last_period': last_period, 'status':STATUS }
	return render(request, 'myidea/idea_detail.html', c)


@csrf_exempt
def like(request,idea_id):
	vote_user = Vote.objects.filter(user__id=request.user.id).values_list('idea_id', flat=True)
	this_idea = Idea.objects.get(id=idea_id)

	if this_idea.id not in vote_user:
		vote = Vote(idea=this_idea, user=request.user, choice=True, vote_date=datetime.now, ip="1.1.1.1")
		vote.save()
		return render(request, 'myidea/like.html')


@csrf_exempt
def dislike(request, idea_id):
	this_idea = Idea.objects.get(id=idea_id)
	vote_user = Vote.objects.filter(user__id=request.user.id).values_list('idea_id', flat=True)

	if this_idea.id not in vote_user:
		vote = Vote(idea=this_idea, user=request.user, choice=False, vote_date=datetime.now, ip="1.1.1.1")
		vote.save()
		return render(request, 'myidea/dislike.html')
	else:
		redirect('idea_detail', idea_id)


def user_idea_list(request, select_status='all_status'):
	if request.user.is_authenticated:
		ideas = Idea.objects.annotate(
			count_vote_y=Count(Case(When(vote__choice=True, then=1))),
			count_vote_n=Count(Case(When(vote__choice=False, then=0)))
		).filter(user_id=request.user.id)

		if select_status in STATUS.keys():
			ideas = ideas.filter(status__name = STATUS[select_status])

		paginator = Paginator(ideas, 10)
		page = request.GET.get('page')
		count_ideas = paginator.count

		try:
			current_page = paginator.page(page)
		except PageNotAnInteger:
			current_page = paginator.page(1)
		except EmptyPage:
			current_page = paginator.page(paginator.num_pages)
	
		c = { 'current_page': current_page, 'status':STATUS, 'select_status': select_status, 'count_ideas':count_ideas }
		return render(request, 'myidea/my/idea_list.html', c)
	else:
		return HttpResponseForbidden()

def user_idea_detail(request, idea_id):
	idea = Idea.objects.get(id=idea_id)
	user_idea = Idea.objects.filter(user__id=request.user.id).values_list('id', flat=True)

	if idea.id in user_idea:
		return render(request, 'myidea/my/idea_detail.html', {'idea':idea, "status":STATUS})
	else:
		return HttpResponseForbidden()


def moderator_idea_list(request, select_status='all_status'):
	ideas = Idea.objects.exclude(status__name=STATUS_APPROVED)

	if select_status in STATUS.keys():
		ideas = ideas.filter(status__name = STATUS[select_status])

	paginator = Paginator(ideas, 10)
	page = request.GET.get('page')
	count_ideas = paginator.count

	try:
		current_page = paginator.page(page)
	except PageNotAnInteger:
		current_page = paginator.page(1)
	except EmptyPage:
		current_page = paginator.page(paginator.num_pages)

	c = {'current_page': current_page, 'status':STATUS, 'select_status': select_status, 'count_ideas':count_ideas}

	return render(request, 'myidea/moderator/idea_list.html', c)



def moderator_idea_detail(request, idea_id):
	idea = Idea.objects.get(id=idea_id)

	approve_form= ApproveForm()
	reject_form = RejectForm()
	result_form = ResultForm()

	data = {'idea': idea,'approve_form':approve_form,'status':STATUS, 'reject_form': reject_form, 'result_form':result_form}
	return render(request, 'myidea/moderator/idea_detail.html', data)


def idea_result_detail(request, idea_id):
	idea = Idea.objects.get(id=idea_id)
	c = {'idea':idea, 'status':STATUS }
	return render(request, 'myidea/idea_detail.html', c)

def idea_review_detail(request, idea_id):
	idea = Idea.objects.get(id=idea_id)
	approve_form= ApproveForm()
	reject_form = RejectForm()
	data = {'idea': idea,'approve_form':approve_form,'status':STATUS, 'reject_form': reject_form,}
	return render(request, 'myidea/moderator/idea_detail.html', data)

def idea_complete_detail(request, idea_id):
	idea = Idea.objects.get(id=idea_id)
	result_form = ResultForm()
	data = {'idea': idea,'result_form':result_form,'status':STATUS}
	return render(request, 'myidea/moderator/idea_detail.html', data)

def approve(request,idea_id):
	idea = Idea.objects.get(id=idea_id)
	approve_form = ApproveForm(request.POST or None)
	reject_form = RejectForm()

	if approve_form.is_valid():
		idea.category = approve_form.cleaned_data['category']
		status_approved = Status.objects.get(name=STATUS_APPROVED)
		idea.status = status_approved
		idea.vote_date = datetime.now()
		idea.user_moderate = request.user
		my_send_mail('idea_approved', idea)
		idea.save()
		return redirect(idea_detail, idea_id)
	else:
		data = {'idea': idea,'approve_form':approve_form,'status':STATUS, 'reject_form': reject_form}
		return render(request, 'myidea/moderator/idea_detail.html', data)

def reject(request,idea_id):
	idea = Idea.objects.get(id=idea_id)
	approve_form = ApproveForm()
	reject_form = RejectForm(request.POST)

	if reject_form.is_valid():
		idea.comment = reject_form.cleaned_data['comment']
		new_status = Status.objects.get(name=STATUS_REJECTED)
		idea.status = new_status
		my_send_mail('idea_rejected', idea)
		idea.save()
		return redirect('/moderator/ideas')
	else:
		data = {'idea': idea,'approve_form':approve_form,'status':STATUS, 'reject_form': reject_form}
		return render(request, 'myidea/moderator/idea_detail.html', data)


def result(request, idea_id):
	idea = Idea.objects.get(id=idea_id)
	result_form = ResultForm(request.POST)

	if result_form.is_valid():
		idea.result = result_form.cleaned_data['result']
		new_status = Status.objects.get(name=STATUS_RESULT)
		idea.status = new_status
		idea.save()
		return redirect('moderator')
	else:
		data = {'idea':idea, 'result_form':result_form, 'status':STATUS}

	return render(request, 'myidea/moderator/idea_detail.html', data)


def logout(request):
	auth.logout(request)
	return redirect('/')


def my_send_mail(mail_category, idea):

	user_email = idea.user.email
	from_email, to = 'evdokimovilya@yandex.ru', user_email

	if mail_category=='idea_review':
		subject = 'Ваша инициатива поступила на проверку'
		html_content = render_to_string('myidea/email/idea_review.html', {'idea':idea})

	elif mail_category=='idea_approved':
		subject = 'Ваша инициатива размещена на голосовании!'
		html_content = render_to_string('myidea/email/idea_approved.html', {'idea':idea})

	elif mail_category == 'idea_rejected':
		subject = 'К сожалению, ваша инициатива не прошла проверку'
		html_content = render_to_string('myidea/email/idea_rejected.html', {'idea':idea})

	elif mail_category == 'idea_complete':
		subject = 'Ваша инициатива прошла голосование, ура !'
		html_content = render_to_string('myidea/email/idea_complete.html', {'idea':idea})

	elif mail_category == 'idea_complete':
		subject = 'Голосование по вашей инициативе завершилось'
		html_content = render_to_string('myidea/email/idea_fail.html', {'idea':idea})


	text_content = strip_tags(html_content) 
	msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()
