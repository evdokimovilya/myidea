# -*- coding: utf-8 -*-
import base64
import json
import requests
from django.conf import settings
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.core.mail import mail_admins
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden

def login_by_authdata(request, data):
	sam_account_name = data.get('samaccountname')
	email_corp = str(data.get('email', '')).lower()
	email_priv = str(data.get('division', '')).lower()
	fio = data.get('displayname')
	# get User object and update
	user, created = User.objects.get_or_create(username=sam_account_name)
	if created:
		user.set_unusable_password()
	if email_corp and (email_corp.find('@at.urfu.ru') == -1):
		user.email = email_corp
	elif email_priv:
		user.email = email_priv
	if fio:
		fio_arr = fio.split(' ', 1)
		user.last_name = fio_arr[0]
		user.first_name = fio_arr[1] if len(fio_arr) == 2 else None
	else:
		user.last_name = sam_account_name
		user.first_name = None
	user.save()
	# user login
	user.backend = 'django.contrib.auth.backends.ModelBackend'
	login(request, user)
	return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

def oauth2login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
	elif request.GET.get('code'):
		params = {
			'grant_type': 'authorization_code',
			'client_id': settings.OAUTH2_SERVICE_URL,
			'redirect_uri': settings.OAUTH2_SERVICE_URL,
			'code': request.GET.get('code'),
		}
		response = requests.post(settings.OAUTH2_TOKEN_URL, data=params).json()
		token = response.get('access_token')
		if token:
			token_parts = token.split('.')
			token_part_userdata = token_parts[1] + '=' * (-len(token_parts[1]) % 4)
			#print (base64.urlsafe_b64decode(token_part_userdata).decode('utf-8'))
			userdata = json.loads(base64.urlsafe_b64decode(token_part_userdata).decode('utf-8'))
			return login_by_authdata(request, userdata)
		else:
			return HttpResponseForbidden(u'Authorization token not found!')
	elif request.GET.get('error'):
		mail_admins('ADFS authentification error', HttpResponse(request.GET.get('error')))
		return HttpResponseForbidden(u'Authentification failed')
	else:
		params = {
			'response_type': 'code',
			'client_id': settings.OAUTH2_SERVICE_URL,
			'redirect_uri': settings.OAUTH2_SERVICE_URL,
			'resource': settings.OAUTH2_SERVICE_URL,
		}
		url = requests.get(settings.OAUTH2_AUTHORIZE_URL, params).url
		return HttpResponseRedirect(url)

def adfs_logout(request):
	logout(request)
	return HttpResponseRedirect(settings.OAUTH2_LOGOUT_URL)

def login_redirect(request):
	return HttpResponseRedirect(settings.OAUTH2_SERVICE_URL)
