
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views

from myidea.adfs.views import login_redirect, oauth2login, adfs_logout

urlpatterns = [
	url(r'^admin/',				admin.site.urls),
	url(r'^auth/adfs$',			oauth2login,		name='urfu_login'),
	url(r'^auth/adfs/',			login_redirect),
	
	url(r'^auth/logout$',		adfs_logout,		name='urfu_logout'),
	url(r'^auth/login/$',       auth_views.login,	{ 'template_name':'myidea/login.html' }, name='login'),
	
	url(r'^', include('myidea.core.urls')),
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
